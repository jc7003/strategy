package com.owen.pattern.strategy;

import com.owen.pattern.strategy.competition.Competition;
import com.owen.pattern.strategy.competition.RunCompetition;

public class Athlete {
	
	private Competition mCompetition; 
	
	public void action(){
		if(mCompetition == null)
			mCompetition = new RunCompetition();
		
		mCompetition.action();
	}
	
	
	public void setCompetition(Competition competition){
		mCompetition = competition;
	}
}
