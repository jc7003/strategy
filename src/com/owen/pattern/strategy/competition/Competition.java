package com.owen.pattern.strategy.competition;

public abstract class Competition {
	
	public abstract void action();

}
