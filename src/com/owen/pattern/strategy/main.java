package com.owen.pattern.strategy;

import com.owen.pattern.strategy.competition.*;

public class main {

	public static void main(String[] args) {
		
		//準備運動員
		Athlete athlete = new Athlete();
		//跑步比賽
		athlete.setCompetition(new RunCompetition());
		athlete.action();
		
		//騎車比賽
		athlete.setCompetition(new RideCompetition());
		athlete.action();
		
		//游泳比賽
		athlete.setCompetition(new SwimCompetition());
		athlete.action();
		
	}

}
